#!/bin/bash
#
# runner.sh
#
# author: dooley@tacc.utexas.edu
#
# Main processing logic for the scripts

# Run it {{{

# Uncomment this line if the script requires root privileges.
# [[ $UID -ne 0 ]] && die "You need to be root to run this script"

# If either credential is missing, force interactive login
if [[ -n "$username" ]] || [[ -n "$access_token" ]]; then
	#echo "Found apikey $apikey apisecret $apisecret"
	interactive=1
else
	#echo "Loooking for stored credentials"
	# Otherwise use the cached credentials if available
	if [ "$disable_cache" -ne 1 ]; then
		if [ -f "$HOME/.xsede" ]; then
			#echo "Found for stored credentials"
			tokenstore=`cat $HOME/.xsede`
			if [ -n "$tokenstore" ]; then
				jsonval username "${tokenstore}" "username"
				jsonval access_token "${tokenstore}" "access_token"
				#echo "Using $username $access_token"
			fi
		fi
	fi

	if [ -z "$access_token" ]; then
		interactive=1
	fi

fi

if ((interactive)); then
  prompt_options
fi

# Adjust the service url for development
filter_service_url

# Force a trailing slash if they didn't specify one in the custom url
hosturl=${hosturl%/}
hosturl="$hosturl/"

# Delegate logic from the `main` function
authheader=$(get_auth_header)
main

# This has to be run last not to rollback changes we've made.
safe_exit

# }}}
