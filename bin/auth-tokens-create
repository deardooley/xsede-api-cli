#!/bin/bash
#
# auth-token-create.sh
#
# author: dooley@tacc.utexas.edu
#
# This script is part of the Agave API command line interface (CLI).
# It retrieves an authentication token from the auth service that
# can be used to authenticate to the rest of the api. A valid API
# secret and key must be used to obtain a token.
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "$DIR/common.sh"

# set to 1 to prevent using cache. token creation requires a valid key
disable_cache=1
storetoken=0

# Script logic -- TOUCH THIS {{{

# A list of all variables to prompt in interactive mode. These variables HAVE
# to be named exactly as the longname option definition in usage().
interactive_opts=(username password)

# Print usage
usage() {
  echo -n "$(basename $0) [OPTION]...

Create a new auth token. Tokens can be restricted to a specific number
of uses and/or timeframe during which they are valid. Tokens can also
have an internal user attached to them. When interacting with systems,
the authentication credentials of the internal user, if available, will
be used instead of those of the API user.

For ease of use, the -S option can be specified to store the new
credential for reuse in subsequent calls to the API.

 Options:
  -u, --username    XUP username
  -p, --password    XUP password. it's recommended to insert this through the interactive option
  -H, --hosturl     URL of the service
  -d, --development Run in dev mode using default dev server
  -f, --force       Skip all user interaction
  -i, --interactive Prompt for values
  -q, --quiet       Quiet (no output)
  -v, --verbose     Output more
  -V, --veryverbose Superverbose output
  -h, --help        Display this help and exit
      --version     Output version information and exit
"
}

##################################################################
##################################################################
#						Begin Script Logic						 #
##################################################################
##################################################################

source "$DIR/auth-common.sh"

main() {
	#echo -n
	#set -x

  cmd="curl -sku \"$username:XXXXXX\" -X POST $hosturl"

  if ((veryverbose)); then
	   log "Calling $cmd"
  fi

	response=`curl -sku "$username:$password" -X POST "$hosturl"`
  
	check_response_status "$response"

	if [ "$response_status" = "success" ]; then
		format_api_json "$response"
    out "$result"
	else
		err $(jsonquery "$response" "message")
	fi


}

format_api_json() {

  access_token=$(jsonquery "$1" "result.[].token")

	if ((storetoken)); then

    # two week lifetime
		expires_in=1209600
		created_at=$(jsonquery "$1" "result.[].created")
    expires_at=$(jsonquery "$1" "result.[].expires")

		echo "{\"baseurl\":\"$baseurl\",\"devurl\":\"$devurl\",\"apisecret\":\"$apisecret\",\"apikey\":\"$apikey\",\"username\":\"$username\",\"access_token\":\"$access_token\",\"refresh_token\":\"$refresh_token\",\"created_at\":\"$created_at\",\"expires_in\":\"$expires_in\",\"expires_at\":\"$expires_at\"}" > ~/.xsede

		echo "Token successfully refreshed and cached for ${expires_in} seconds";
	fi

  if ((verbose)); then
    echo "$1" | python -mjson.tool
	else
		echo "$access_token"
	fi
}

##################################################################
##################################################################
#						End Script Logic						 #
##################################################################
##################################################################

# }}}

# Parse command line options
source "$DIR/options.sh"


# Main loop {{{

# Print help if no arguments were passed.
[[ $# -eq 0 ]] && set -- "-i"

# Read the options and set stuff
while [[ $1 = -?* ]]; do
  case $1 in
    -h|--help) usage >&2; safe_exit ;;
    --version) out "$(basename $0) $version"; safe_exit ;;
    -u|--username) shift; username=$1 ;;
  	-p|--password) shift; password=$1 ;;
    -S|--storetoken) storetoken=1 ;;
	  -H|--hosturl) shift; hosturl=$1;;
  	-d|--development) development=1 ;;
    -v|--verbose) verbose=1 ;;
    -V|--veryverbose) veryverbose=1; verbose=1 ;;
    -q|--quiet) quiet=1 ;;
    -i|--interactive) interactive=1 ;;
    -f|--force) force=1 ;;
    --endopts) shift; break ;;
    *) die "invalid option: $1" ;;
  esac
  shift
done

# Store the remaining part as arguments.
args+=("$@")

# }}}

# Run the script logic
source "$DIR/runner.sh"
