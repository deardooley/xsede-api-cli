#!/bin/bash
# 
# auth-common.sh
# 
# author: dooley@tacc.utexas.edu
#
# URL filter for auth services
#

filter_service_url() {
	if [[ -z $hosturl ]]; then
		if ((development)); then 
			hosturl="${devurl}tokens"
		else
			hosturl="${baseurl}tokens"
		fi
	fi
	
	hosturl=${hosturl%/}
}

