# The Unofficial XSEDE API Command Line Interface

This is a CLI for the unofficial XSEDE REST APIs. This CLI handles authentication, token creation, system querying, and job queries. It is not meant to be a complete CLI, but rather an ongoing work in progress. Use it at your own risk. If you find value in the CLI, please let us know so we can use that to request additional resources. If you wind up adding enhancements,  please make a pull request so we can include them and give you proper credit.

## What is XSEDE

XSEDE is a single virtual system that scientists can use to interactively share computing resources, data and expertise. People around the world use these resources and services — things like supercomputers, collections of data and new tools — to improve our planet.

Read more about XSEDE on the official [website](http://www.xsede.org) and [user portal](https://portal.xsede.org).

## Installation from source

The following technologies are required to use the Agave API cli tools.

	* bash
	* curl
	* Perl
	* Python (including json.tool)

Just clone the repository from Bitbucket and add the bin directory to your classpath and you're ready to go.

	> git clone https://bitbucket.org/deardooley/xsede-api-cli.git xsede-api-cli
	> export PATH=$PATH:`pwd`/xsede-api-cl/bin

If you intend on using the CLI on a regular basis, you should add the above path command to your `.bashrc` or `.profile`.

## Getting started

From here on, we assume you have the CLI installed and your environment confired properly. We also assume you either set or will replace the following environment variables:

* `XUP_USERNAME`: The username you use to login to the [XSEDE User Portal](https://portal.xsede.org).
* `XUP_PASSWORD`: The password you use to login to the [XSEDE User Portal](https://portal.xsede.org).


### Authentication

In order to communicate with the unnoficial XSEDE REST API, you need to obtain an access token. The CLI comes with a script to do this for you. Simply run the `auth-tokens-create` command and supply your `XUP_USERNAME` and `XUP_PASSWORD`. Alternatively, you can specify an access token at the command line by providing the `-t` or `--access_token` option. If you would like to store a token for repeated use so you don't have to keep reauthenticating with every call, run the `auth-tokens-create` script with the `-S` option to store the token locally for future use.

	> auth-tokens-create -S -u "$XUP_USERNAME" -p "$XUP_PASSWORD"

Your API token will be valid for 14 days. There is no refresh mechanism. Just create a new one.

### Using the CLI

The Agave CLI is broken down into the following groups of scripts

	- auth*           authenticate
	- jobs*           list and query current job information
	- systems*        query for system information

All commands follow a common syntax and share many of the same flags `-h` for help, `-d` for debug mode, `-v` for verbose output, `-V` for very verbose (prints curl command and full service response), and `-i` for interactive mode. Additionaly, individual commands will have their own options specific to their functionality. The general syntax all commands follow is:

	<command> [-dhivV] [options] [target]

Each command has built in help documentation that will be displayed when the `-h` or `--help` flag is specified. The help documentation will list the actions, options, and targets available for that command.
 
